---
title: Prolog Pathfinding
subtitle: Different pathfinding algorithms in prolog
description: "This is a study project in the subject AI. In prolog different pathfinding algorithms were implmented. There are different rooms locked by keys."
proj_link: https://gitlab.com/OlMon/prolog-pathfinding
img_folder: prolog_pathfinding
image: prolog_pathfinding.png
show: True
---
