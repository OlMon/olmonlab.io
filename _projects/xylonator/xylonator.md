---
title: Xylonator
subtitle: Xylonator - The music roboter
description: "Study project in robotics, letting a Kuka roboter play xylophone from midi and spreadsheet files"
proj_link: https://gitlab.com/OlMon/xylonator
img_folder: xylonator
image: xylonator.png
show: True
---
