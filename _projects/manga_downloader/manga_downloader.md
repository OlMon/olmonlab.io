---
title: Manga Downloader
subtitle: App to download manga as PDF
description: "This Software can download mangas from websites as PDF file. It is easiely extendable through dynamically loading python classes."
proj_link: https://gitlab.com/OlMon/manga-downloader
img_folder: manga_downloader
image: manga_dl.png
show: True
---
