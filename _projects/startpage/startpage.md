---
title: Startpage
subtitle: custom browser startpage
description: "A small website created from an org file, to create a custom startpage for a webbrowser."
proj_link: https://gitlab.com/OlMon/startpage
img_folder: startpage
image: startpage.png
show: True
---
