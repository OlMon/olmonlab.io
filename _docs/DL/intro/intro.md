---
title: Introduction -  Deep Learning
permalink: /docs/dl/intro/
img_folder: dl/intro

---

Deep Learning is a subfield of Machine Learning that uses artificial neural network as algorithm,
to learn to predict a specific output from a given input.

Artificial neural networks are loosely based on neural network in a human brain. Every neuron has
an input activation threshold and output function.

The root of artificial neural network is probably the perceptron that was already invented in 1958
by Frank Rosenblatt.
The perceptron was only a linear classifier.

With the invention of the backpropagation algorithm, the availability of powerful computer and
access to huge amount of data, neural network could show their real potential.

The AlexNet was one of the first to present the capabilities of neural networks for
image classification.

Besides image classification with convolutional neural networks, other types of network
were developed for different type of problems.

Deep Learning is currently an active studied field with huge potential. Recently a neural network
was able to achieve a big milestone in the medical subject of protein folding.

![neural_network]({{page.img}}/{{page.img_folder}}/neural_network.png)
*Neural Network structure*
