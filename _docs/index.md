---
title: Docs
permalink: /docs/home/
redirect_from: /docs/index.html
---

These are the docs. Here are short summaries about topics I learned.
Feel free to contact me if you want to extend something or if you find a mistake.

### Topics

I focus mostly on three topics in my research: Artificial Intelligence, Machine Learning and Deep learning. I try to distinguish between these topics as much as possible.

### Navigation

To find faster what you are looking for, there is a navigation on the left side. Additionally you can search for topics with the search box at the top of the side.

