//open doc navigation to selected section
$(function () {
    $('div.collapse').each(
        function(){
            if( $(this).find('a.cur-doc').length != 0) $(this).addClass('show')
              });
})

//set chevron to right position on init
$(function () {
    $('button.list-group-item').each(
        function(){
            if( $(this).next('.show').length != 0 )  $(this).find('span.fa')
            .removeClass('fa-chevron-right')
            .addClass('fa-chevron-down');
              });
  })

$(function() {

    //var myParam = location.href.search.split('title=')[1]

    var url = location.href; // or window.location.href for current url
    var myParam = /title=([^&]+)/.exec(url); // Value is in [1] ('384' in our case)
    
    if(myParam) {
        if(myParam[1]) {
            var card_title = document.getElementsByClassName("card");
            for(var i = 0; i < card_title.length; i++){
                if(card_title[i].firstElementChild.firstElementChild.innerText === myParam[1]){
                    card_title[i].className += " searched-proj";
                    card_title[i].focus();
                }
            }
        }
    }
})

$(function() {

    // $('.collapse').collapse('hide');
    $('.list-group-item.active').parent().parent('.collapse').collapse('show');


    var pages = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
        // datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        prefetch: baseurl + '/js/search.json'
    });

    $('#search-box').typeahead({
        minLength: 0,
        highlight: true
    }, {
        limit: 100,
        name: 'pages',
        source: pages,
        templates: {
            
            suggestion: function(item) {console.log(item);
                                        if(item.title.length >= 30) {
                                            item.title = item.title.substring(0,30) + '..  ';
                                        };
                                        if(item.type === 'doc') {
                                            return('<div><span class="search-title">'+ item.title +'</span><span class="badge badge-success">&nbsp;&nbsp;&nbsp;'+ item.type +'&nbsp;&nbsp;&nbsp;</span></div>');
                                        } else if(item.type === 'paper') {
                                            return('<div><span class="search-title">'+ item.title +'</span><span class="badge badge-info">&nbsp;'+ item.type +'&nbsp;</span></div>');
                                        } else if(item.type === 'project') {
                                            return('<div><span class="search-title">'+ item.title +'</span><span class="badge badge-secondary">'+ item.type +'</span></div>');
                                        }}
        }
    });

    $('#search-box').bind('typeahead:select', function(ev, suggestion) {
        window.location.href = suggestion.url;
    });


    // Markdown plain out to bootstrap style
    $('#markdown-content-container table').addClass('table');
    $('#markdown-content-container img').addClass('img-responsive');


});


// Toggle angle icon in listgroups
$(function() {
    $('button.list-group-item').on('click', function() {
        $('.fa', this)
            .toggleClass('fa-chevron-right')
            .toggleClass('fa-chevron-down');
    });
});

