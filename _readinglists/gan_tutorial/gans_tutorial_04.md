---
title: How to Implement the Inception Score (IS) for Evaluating GANs
date: 27.12.2020
source: https://machinelearningmastery.com/how-to-implement-the-inception-score-from-scratch-for-evaluating-generated-images/
released:
author: Jason Brownlee
topic: DL
tags: [GANs, Tutorial]
format: article
img_folder : 
show: True
---

In the series a gentle introduction to GANs, a short explanation about the inception score and how to calculate it.
