---
title: How to Evaluate Generative Adversarial Networks
date: 27.12.2020
source: https://machinelearningmastery.com/how-to-evaluate-generative-adversarial-networks/
released:
author: Jason Brownlee
topic: DL
tags: [Tutorial, GANs]
format: article
img_folder : 
show: True
---

From the GANs tutorial about how to evaluate a GAN.
