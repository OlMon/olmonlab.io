---
title: Yann LeCun’s Deep Learning Course at CDS
date: 27.12.2020
source: https://cds.nyu.edu/deep-learning/
released:
author:
topic: DL
tags: [deep learning, tutorial]
format: book
img_folder : 
show: True
---
Yann LeCun's course to deep learning
