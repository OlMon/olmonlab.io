---
title: OpenSpiel, A Framework for ReinforcementLearning in Games
date: 27.12.2020 
source: https://arxiv.org/abs/1908.09453
released:
author:
topic: AI
tags: [enviroment, ai]
format: paper
img_folder : 
show: True
---

Developed by DeepMind, a collection of enviroments and algorithms for researchers. It includes reinforcement learning and search & planning algorithms. This collection includes games with different characteristics, like n-player, turnbased, perfect and imperfect knowledge and others. 
